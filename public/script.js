// Initialize Firebase
const config = {
  apiKey: "AIzaSyAzwnjQ3p5KdnXbOT7_ykUcglURCPCdECE",
  authDomain: "vocupapp.firebaseapp.com",
  databaseURL: "https://vocupapp.firebaseio.com",
  projectId: "vocupapp",
  storageBucket: "vocupapp.appspot.com",
  messagingSenderId: "1054492766644"
};
firebase.initializeApp(config);
const storage = firebase.storage().ref();
const database = firebase.database().ref();

let puid = {};
puid.generate = () => {
  let d = new Date().getTime();
  d += performance.now();
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    let r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
};

const video = document.getElementById('video');
const canvas = document.getElementById('canvas');
const response = document.getElementById('response');
const other = document.getElementById('other');
const otherC = document.getElementById('other-context');
const definition = document.getElementById('definition');
const definitionPH = document.getElementById('definition-placeholder');
const defClose = document.getElementById('close-button');

let front = false;
let mediaStreamTrack;
let active = false;

let constraints = { 
  video: { 
      facingMode: "environment", 
      width: { exact: 720 },
      height: { exact: 720 } 
    } 
  };

function camera() {
  constraints = { 
  video: { 
      facingMode: (front? "user" : "environment"), 
      width: { exact: 720 },
      height: { exact: 720 } 
    } 
  };
  navigator.mediaDevices.getUserMedia(constraints)
  .then(gotMedia)
  .catch(error => console.error('getUserMedia() error:', error));
}

camera();

function gotMedia(stream) {
  mediaStreamTrack = stream.getVideoTracks()[0];
  video.srcObject = stream;
  
}

function goBack() {
  active = false;
  start.style.display = 'initial';
  setTimeout(() => {start.style.opacity = '1'},100);
  error.style.display = 'none';
  error.style.opacity = '0';
  uploading.style.display = 'none';
  uploading.style.opacity = '0';
  asking.style.display = 'none';
  asking.style.opacity = '0';
  choose.style.display = 'none';
  choose.style.opacity = '0';
  progressbar.style.display = 'none';
  flip.style.opacity = '1';
  back.style.opacity = '0';
  capture.style.opacity = '1';
  canvas.style.opacity = '0';
  response.textContent = '';
  otherC.textContent = '';
  other.classList.remove('show');
  canvas.getContext('2d').clearRect(0, 0, 1200, 1000)
  camera();
}

function define(word) {
  definitionPH.classList.add('loading');
  definitionPH.classList.add('show');

  const h2 = document.createElement('h2');
  h2.textContent = word;
  definition.appendChild(h2);

  let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
      definitionPH.classList.remove('loading');
      console.log(xhr.response);

      try {
        const data = JSON.parse(xhr.response);

        data.forEach(object => {
          let phonetic = document.createElement('p');
          phonetic.className = 'phonetic';
          phonetic.textContent = object.phonetic;
          definition.appendChild(phonetic);

          for (const prop in object.meaning) {
            object.meaning[prop].forEach(meaning => {
              let def = document.createElement('p');
              def.className = 'definition';
              def.textContent = meaning.definition;
              definition.appendChild(def);

              let expl = document.createElement('p');
              expl.className = 'example';
              expl.textContent = meaning.example;
              definition.appendChild(expl); 
            });
          }
        });
      } catch(e) {
        let no = document.createElement('p');
        no.className = 'no';
        no.textContent = 'Bu kelime sözlükte yok :/';
        definition.appendChild(no);
      }
    }
  };
  xhr.open("POST", "hello", true);
  xhr.send(word);
};

defClose.onclick = () => { 
  definitionPH.classList.remove('show');
  definition.classList.remove('loading');
  definition.textContent = '';
};

definitionPH.onclick = () => { 
  definitionPH.classList.remove('show');
  definition.classList.remove('loading');
  definition.textContent = '';
};

const start = document.getElementById("start");
const asking = document.getElementById("asking");
const uploading = document.getElementById("uploading");
const error = document.getElementById("error");
const choose = document.getElementById("choose");
const capture = document.getElementById('capture-button');
const flip = document.getElementById('flip-button');
const back = document.getElementById('back-button');
const back2 = document.getElementById('back-button2');
const progressbar = document.getElementById('progressbar');


flip.onclick = () => { 
  front = !front; 
  mediaStreamTrack.stop();
  camera();
};

capture.onclick = () => { 
  const imageCapture = new ImageCapture(mediaStreamTrack);
  const genPuid = puid.generate();

  active = true;

  start.style.display = 'none';
  start.style.opacity = '0';
  uploading.style.display = 'initial';
  setTimeout(() => {uploading.style.opacity = '1'},100);
  progressbar.style.display = 'initial';
  flip.style.opacity = '.3';
  back.style.opacity = '1';
  capture.style.opacity = '.3';
  canvas.style.opacity = '1';

  imageCapture.grabFrame()
    .then(imageBitmap => {
      canvas.width = imageBitmap.width;
      canvas.height = imageBitmap.height;
      canvas.getContext('2d').drawImage(imageBitmap, 0, 0);
      mediaStreamTrack.stop();

      const base64Img = canvas.toDataURL("image/jpeg", 0.8);
      const data = base64Img.replace(/^data:image\/(png|jpg);base64,/, "");
      const blob = new Blob([data], {type: 'text/plain'});

      storage.child(genPuid + '.jpg').putString(base64Img, 'data_url').then(snapshot => {
        if (active) {
          uploading.style.display = 'none';
          uploading.style.opacity = '0';
          asking.style.display = 'initial';
          setTimeout(() => {asking.style.opacity = '1'},100);
        }
      });

      database.child(genPuid).on('value', snapshot => {
        const val = snapshot.val();
        console.log(val);
        if (active && val) {
          asking.style.display = 'none';
          asking.style.opacity = '0';
          choose.style.display = 'initial';
          setTimeout(() => {choose.style.opacity = '1'},100);
          progressbar.style.display = 'none';

          if (val.objects) {
            val.objects.forEach(object => {
              const left = object.boundingPoly.normalizedVertices[0].x * 100;
              const top = object.boundingPoly.normalizedVertices[0].y * 100;
              const width = object.boundingPoly.normalizedVertices[1].x * 100 - left;
              const height = object.boundingPoly.normalizedVertices[3].y * 100 - top;

              const div = document.createElement('div');
              div.className = 'object word';
              div.style.left = left + '%';
              div.style.top = top + '%';
              div.style.width = width + '%';
              div.style.height = height + '%';

              const p = document.createElement('p');
              p.textContent = object.name;

              div.appendChild(p);

              const br = document.createElement('br');
              p.appendChild(br);

              const span = document.createElement('span');
              span.textContent = ' ' + val.translations[object.name];

              p.appendChild(span);

              response.appendChild(div);
              div.onclick = () => { 
                define(object.name);
              }
            });
          }

          other.classList.add('show');

          val.labels.forEach(object => {
            const p = document.createElement('p');
            p.className = 'label word';
            p.textContent = object.description;

            const span = document.createElement('span');
            span.textContent = ' ' + val.translations[object.description];

            p.appendChild(span);

            otherC.appendChild(p);
            p.onclick = () => { 
              define(object.description);
            }
          });
        }
      });
    })
    .catch(error => {
      console.error('grabFrame() error:', error);
      error.style.display = 'initial';
      error.style.opacity = '1';
      uploading.style.display = 'none';
      uploading.style.opacity = '0';
      asking.style.display = 'none';
      asking.style.opacity = '0';
      progressbar.style.display = 'none';

    });
};

back.onclick = () => {
  goBack();
};

back2.onclick = () => {
  goBack();
};