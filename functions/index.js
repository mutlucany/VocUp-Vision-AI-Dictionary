const admin = require('firebase-admin');
const functions = require('firebase-functions');
const vision = require('@google-cloud/vision');
const {Translate} = require('@google-cloud/translate');
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

var app = admin.initializeApp();

exports.callVision = functions.storage.object().onFinalize(async (object) => {
    const gcsUrl = "gs://" + object.bucket + "/" + object.name;
    const puid = object.name.replace(".jpg", "");

    let text = [];

    const client = new vision.ImageAnnotatorClient();

    const [result] = await client.labelDetection(gcsUrl);
    const labels = result.labelAnnotations;

    labels.forEach(label => text.push(label.description));

    const [result2] = await client.objectLocalization(gcsUrl);
    const objects = result2.localizedObjectAnnotations;

    objects.forEach(object => text.push(object.name));

    const translate = new Translate();

    const options = {
      to: 'tr',
    };

    let tr = {}

    let [translations] = await translate.translate(text, options);
    translations = Array.isArray(translations) ? translations : [translations];
    translations.forEach((translation, i) => {
      tr[text[i]] = translation;
    });

    app.database().ref(puid).set({
        labels: labels,
        objects: objects,
        translations: tr
    });
});

exports.hello = functions.https.onRequest((req, res) => {
    let xhr = new XMLHttpRequest();
  xhr.onreadystatechange = () => {
    if (xhr.readyState === 4) {
        res.send(xhr.responseText);
    }
  };
xhr.open("GET", "https://mydictionaryapi.appspot.com/?define="+req.body, true);
xhr.send()

});