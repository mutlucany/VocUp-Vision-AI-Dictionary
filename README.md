# VocUp
Dictionary app to look up the name and definitions of objects with the camera using AI Vision.

Firebase Cloud Functions and Google Cloud Vision API are used for this app.

![Screenshoot](screenshots.png)